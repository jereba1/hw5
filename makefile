CC = gcc
C_FLAGS = -c
OBJECTS = $(addsuffix .o,$(SRC)) # list of all .o files
PGM = keeplog
SRC1 = $(wildcard *.c) # list of all .c files
SRC = $(patsubst %.c,%,$(SRC1)) # list of all .c files with .c stripped off

all: $(PGM)

$(PGM): $(OBJECTS)

$(OBJECTS): %.o: %.c
	$(CC) $(C_FLAGS) $< -o $@
	$(CC) -MM $< > $*.d

-include $(addsuffix .d,$(SRC))

clean:
	@-rm -f  *.o *.d $(PGM)
